#!/bin/bash

schema_file=/tmp/init_s4m_data/schema.sql
unchanging_tables_file=/tmp/init_s4m_data/unchanging_tables.sql

# This is for the schema
psql -U portaladmin portal_beta -f "$schema_file"


#This is for the unchanged tables
psql -U portaladmin portal_beta -f "$unchanging_tables_file"
