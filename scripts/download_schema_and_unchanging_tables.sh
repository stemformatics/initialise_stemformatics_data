#!/bin/bash
schema_file=/tmp/init_s4m_data/schema.sql
unchanging_tables_file=/tmp/init_s4m_data/unchanging_tables.sql

unchanging_tables="genome_annotations annotation_databases assay_platforms stemformatics.feature_mappings stemformatics.transcript_annotations"
unchanging_tables_command=`for table in $unchanging_tables; do echo "-t $table "; done`

#Changing tables
#dataset_metadata
#datasets
#biosamples_metadata


pg_dump -U portaladmin portal_prod -s > "$schema_file"

pg_dump -U portaladmin portal_prod --data-only $unchanging_tables_command > "$unchanging_tables_file"


wc -l  $schema_file
wc -l  $unchanging_tables_file

