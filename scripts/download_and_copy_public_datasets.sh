#!/bin/bash

#1. Get a list of public datasets

#2. Dump out all the tables for the public datasets only
#datasets
#dataset_metadata
#biosamples_metadata

#3. Copy all the files across
#yugene
#rohart
#gct
#probes
#
#NOTE: cls is obsolete and standard deviation will be obsolete.
copy_dir_base=/tmp/init_s4m_data
mkdir "${copy_dir_base}"
mkdir "${copy_dir_base}"

file_dir_base=/var/www/pylons-data/prod
list_of_public_datasets="/tmp/init_s4m_data/list_of_public_datasets.txt"
datasets_dump="/tmp/init_s4m_data/datasets.sql"
dataset_metadata_dump="/tmp/init_s4m_data/dataset_metadata.sql"
biosamples_metadata_dump="/tmp/init_s4m_data/biosamples_metadata.sql"
today=`date '+%Y_%m_%d__%H_%M_%S'`;
tmpfile="/tmp/dump_init_files$today"
limit=20

psql -U portaladmin portal_prod -t  -P "footer=off" -c "select id from datasets where not private order by id limit $limit;" > "$list_of_public_datasets"

#Delete empty rows
sed -i '/^$/d' "$list_of_public_datasets"

# Delete the spaces in the file
sed -i 's/ //g' "$list_of_public_datasets"

#Now go and grab the file and create a list for me to use in sql
csv_of_datasets=""

while IFS= read -r ds_id
do
    csv_of_datasets+="$ds_id,"  
    # Now get the files
    #CUMULATIVEFiles/dataset4000.cumulative.txt
    #RohartFilesMSC/dataset4000.rohart.MSC.txt
    #GCTFiles/dataset4000.gct
    #ProbeFiles/4000.probes
    cp ${file_dir_base}/CUMULATIVEFiles/dataset${ds_id}.cumulative.txt "$copy_dir_base"
    cp ${file_dir_base}/RohartFilesMSC/dataset${ds_id}.rohart.MSC.txt "$copy_dir_base"
    cp ${file_dir_base}/GCTFiles/dataset${ds_id}.gct "$copy_dir_base"
    cp ${file_dir_base}/ProbeFiles/${ds_id}.probes "$copy_dir_base"


done < "$list_of_public_datasets"

# Remove last , from the list
csv_of_datasets=${csv_of_datasets%?};

echo $csv_of_datasets

psql -U portaladmin portal_prod -c "COPY (select * from datasets where id in ($csv_of_datasets)) TO '$tmpfile';";
cp $tmpfile $datasets_dump

psql -U portaladmin portal_prod -c "COPY (select * from dataset_metadata where ds_id in ($csv_of_datasets)) TO '$tmpfile';";
cp $tmpfile $dataset_metadata_dump

psql -U portaladmin portal_prod -c "COPY (select * from biosamples_metadata where ds_id in ($csv_of_datasets)) TO '$tmpfile';";
cp $tmpfile $biosamples_metadata_dump



