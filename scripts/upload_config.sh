#!/bin/bash

copy_dir_base=/tmp/init_s4m_data
config_dump_file=$copy_dir_base/config.sql

psql -U portaladmin portal_beta -c "copy stemformatics.configs from '$config_dump_file'"

psql -U portaladmin portal_beta -c "insert into stemformatics.configs values(default,'feedback_email','stemformatics@mailinator.com');"

