#!/bin/bash 
# this is what needs to be run in which order

base_data_dir=/tmp/init_s4m_data
tar_zip_file=/tmp/initialise_stemformatics_data_from_portal.tgz
base_file_dir=/var/www/pylons-data/prod
base_file_dir=/tmp/prod_files

wget "https://portal.stemformatics.org/downloads/initialise_stemformatics_data.tgz" -O "$tar_zip_file"

rm -fR "$base_data_dir"
mkdir "$base_data_dir"

tar zxvf "$tar_zip_file" --strip-components=2 -C "$base_data_dir"

ls -alh "$base_data_dir"


# Reset the database using portal_beta for now
psql -U portaladmin postgres -c "drop database portal_beta;"
psql -U portaladmin postgres -c "create database portal_beta;"


echo "Setup schema and unchanging tables like genome_annotation"
./upload_schema_and_unchanging_tables.sh
echo "Setup configurations"
./upload_config.sh
echo "Setup datasets"
./upload_public_datasets.sh
echo "Setup guest"
./upload_guest_user.sh


# Now get the files
if [ ! -d "$base_file_dir" ]; then
    mkdir -p "$base_file_dir"
fi

for directory in GCTFiles RohartMSCFiles CUMULATIVEFiles ProbeFiles ;
do
    rm -fR "${base_file_dir}/${directory}"
    mkdir "${base_file_dir}/${directory}"

done


cp ${base_data_dir}/*.gct "${base_file_dir}/GCTFiles/"
cp ${base_data_dir}/*cumulative* "${base_file_dir}/CUMULATIVEFiles/"
cp ${base_data_dir}/*.probes "${base_file_dir}/ProbeFiles/"
cp ${base_data_dir}/*rohart* "${base_file_dir}/RohartMSCFiles/"


./run_redis_from_scratch.sh
