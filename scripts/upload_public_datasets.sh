#!/bin/bash

datasets_dump="/tmp/init_s4m_data/datasets.sql"
dataset_metadata_dump="/tmp/init_s4m_data/dataset_metadata.sql"
biosamples_metadata_dump="/tmp/init_s4m_data/biosamples_metadata.sql"

psql -U portaladmin portal_beta -c "copy datasets from '$datasets_dump'"

psql -U portaladmin portal_beta -c "copy dataset_metadata from '$dataset_metadata_dump'"

psql -U portaladmin portal_beta -c "copy biosamples_metadata from '$biosamples_metadata_dump'"

