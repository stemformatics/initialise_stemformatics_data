#!/bin/bash

tmpfile="/tmp/dump_init_files$today"

copy_dir_base=/tmp/init_s4m_data

config_dump_file=$copy_dir_base/config.sql

ignore_configs="'twitter_app_key','twitter_app_secret','twitter_oauth_token','twitter_oauth_token_secret','email_to','error_email_from','feedback_email','galaxy_server_api_key','galaxy_server_url','google_analytics_tracking_id','publish_gene_set_email_address','recaptcha.private_key','smtp_password','smtp_server','from_email'"

psql -U portaladmin portal_prod -c "COPY (select * from stemformatics.configs where ref_type not in ($ignore_configs)) TO '$tmpfile';";
cp $tmpfile $config_dump_file


