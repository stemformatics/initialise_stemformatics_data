--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: stemformatics; Type: SCHEMA; Schema: -; Owner: portaladmin
--

CREATE SCHEMA stemformatics;


ALTER SCHEMA stemformatics OWNER TO portaladmin;

--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: portaladmin
--

CREATE PROCEDURAL LANGUAGE plpgsql;


ALTER PROCEDURAL LANGUAGE plpgsql OWNER TO portaladmin;

SET search_path = public, pg_catalog;

--
-- Name: ens_id_status; Type: TYPE; Schema: public; Owner: portaladmin
--

CREATE TYPE ens_id_status AS ENUM (
    'retired',
    'remapped'
);


ALTER TYPE public.ens_id_status OWNER TO portaladmin;

--
-- Name: abacus_db_simple_table_mod_stamp(); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION abacus_db_simple_table_mod_stamp() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	DECLARE
		Vmaj INT := version_part(1);
		Vmin INT := version_part(2);
		tbl_name TEXT := CASE WHEN Vmaj = 7 OR Vmaj = 8 AND Vmin < 2 THEN TG_RELNAME ELSE TG_TABLE_NAME END;
	BEGIN
		INSERT INTO abacus_db_audit_log
			SELECT NOW(),TG_OP,current_user,tbl_name || ' was modified.';
		RETURN NULL;
	END;
$$;


ALTER FUNCTION public.abacus_db_simple_table_mod_stamp() OWNER TO portaladmin;

--
-- Name: clearance(text, integer); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION clearance(act text, subj integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE 
	group_list int[] := '{}';
	perm_list int[] := '{}';
	perms_sublist int [] := '{}';
	act_perms int [] := '{}';
	perm_code bigint;
	act_perm_code int;
	zz int;

BEGIN
	-- First, find all the groups of which this subject is a member.
	-- If there are no such groups, then return a permission code that
	-- is NEVER EVER USED.

	group_list := find_groups(subj);
	-- RAISE NOTICE 'group: %', group_list; -- DEBUG
	-- RAISE NOTICE 'lower %', array_lower(group_list,1); -- DEBUG
 	IF group_list IS NULL OR array_lower(group_list,1) IS NULL THEN
	    return FALSE;
	END if;

	-- Subj is a member of at least one group.  Retrieve the set of permssions granted
	-- to this subj by virtue of membership in these groups.

	-- RAISE NOTICE 'group: %', group_list; -- DEBUG
	FOR zz IN array_lower(group_list,1) .. array_upper(group_list, 1) LOOP
	    perm_list := array_cat(perm_list, 
	    	  array(
	    	      SELECT permissions.prime_factor FROM role_assignments 
		      	     JOIN role_permissions ON (role_assignments.role = role_permissions.role) 
			     JOIN permissions ON (role_permissions.mnemonic=permissions.mnemonic) 
			     WHERE role_assignments.sid=group_list[zz] 
			     GROUP BY permissions.prime_factor
			 )
		);
	    -- RAISE NOTICE 'group %   perm_list %', group_list[zz], perm_list; -- DEBUG
	END LOOP;

	-- Reduce the list to a set of unique (i.e. non-duplicate) values, then
	-- compute their product.  
	-- (Yes, this looks UGLY, but I found it on-line and it works.)

	perms_sublist := array(
		      SELECT DISTINCT perm_list[s.i] AS pf  
		      	     FROM generate_series(array_lower(perm_list,1), array_upper(perm_list,1)) AS s(i)
			     );

	-- RAISE NOTICE 'perms_sublist %', perms_sublist; -- DEBUG

	IF perms_sublist IS NULL OR array_lower(perms_sublist,1) IS NULL THEN
	    RETURN FALSE;
	END IF;

	perm_code = 1;
	FOR zz in array_lower(perms_sublist,1) .. array_upper(perms_sublist,1) LOOP
	    perm_code := perm_code * perms_sublist[zz];
	END LOOP;

	-- Now that we have the permission code, go fetch this activity's permissions,
	-- according to the roles associated with the activity, and produce a similar
	-- code for each of these.  Then iterate through this list of codes until
	-- we find one that divides evenly into perm_code, returning true if we find one
	-- or false if we make it through the whole list without finding one.

	act_perms = array(SELECT DISTINCT permissions.prime_factor FROM activities 
	    		    	   JOIN role_associations ON (activities.activity_id=role_associations.activity_id)
	    		    	   JOIN role_permissions ON (role_associations.role=role_permissions.role)
				   JOIN permissions ON (role_permissions.mnemonic=permissions.mnemonic)
				   WHERE activities.name=act
			);
	
	-- RAISE NOTICE 'act_perms %', act_perms; -- DEBUG
 	IF act_perms IS NULL OR array_lower(act_perms,1) IS NULL THEN
	    RETURN FALSE;
	END if;

	act_perm_code = 1;
	FOR zz in array_lower(act_perms,1) .. array_upper(act_perms,1) LOOP
	    act_perm_code := act_perm_code * act_perms[zz];
	END LOOP;

	RETURN  mod(perm_code, act_perm_code) = 0;
END;
$$;


ALTER FUNCTION public.clearance(act text, subj integer) OWNER TO portaladmin;

--
-- Name: del_subject_trig(); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION del_subject_trig() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
        n_sid INTEGER;
	Vmaj INT := version_part(1);
	Vmin INT := version_part(2);
	tbl_name TEXT := CASE WHEN Vmaj = 7 OR Vmaj = 8 AND Vmin < 2 THEN TG_RELNAME ELSE TG_TABLE_NAME END;
	BEGIN
	    RAISE NOTICE 'tbl_name: %', tbl_name;
	    IF (tbl_name = 'subscribers') THEN
		    DELETE FROM subjects WHERE sid=OLD.sid;
		    RETURN NULL;
		ELSEIF (tbl_name = 'groups') THEN
		    DELETE FROM subjects WHERE sid=OLD.gid;
            RETURN NULL;
		ELSE
		    RAISE WARNING 'Trigger called on wrong table %', tbl_name;
		    RETURN NULL;
		END IF;
	END;
$$;


ALTER FUNCTION public.del_subject_trig() OWNER TO portaladmin;

--
-- Name: drop_genome_annotation(); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION drop_genome_annotation() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    begin
       delete from genome_annotations where db_id = OLD.db_id
           and gene_id = OLD.gene_id;  
       delete from genome_locations where database_id = OLD.db_id 
           and identifier = OLD.gene_id;
       delete from genome_mappings where annotator_db1 = OLD.db_id 
           and annotator_id1 = OLD.gene_id;

       return null;
    end;

$$;


ALTER FUNCTION public.drop_genome_annotation() OWNER TO portaladmin;

--
-- Name: drop_probe_annotation(); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION drop_probe_annotation() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    begin

         delete from probe_annotations  where chip_type = old.chip_type and 
             probe_id = old.probe_id;

         delete from probe_mappings  where chip_type = old.chip_type and 
             probe_id = old.probe_id;

         delete from probe_locations  where chip_type = old.chip_type and 
             probe_id = old.probe_id;

         return null;

    end;
$$;


ALTER FUNCTION public.drop_probe_annotation() OWNER TO portaladmin;

--
-- Name: find_groups(integer); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION find_groups(integer) RETURNS integer[]
    LANGUAGE plpgsql
    AS $_$
DECLARE 
	group_list integer[ ] := '{}';
	zz integer;
BEGIN
	FOR zz IN SELECT gid FROM group_membership WHERE sid=$1 GROUP BY gid LOOP
	    -- RAISE NOTICE 'group: %', zz;
	    group_list = array_append(group_list, zz.gid);
	    group_list =  array_cat(group_list, array(
	    	       SELECT gid FROM group_membership 
		       	      WHERE sid=zz GROUP BY gid
			)
		);
	    -- RAISE NOTICE 'zz %   foo %', zz, foo;
	END LOOP;
	RETURN group_list;
END;
$_$;


ALTER FUNCTION public.find_groups(integer) OWNER TO portaladmin;

--
-- Name: find_perms(integer); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION find_perms(integer) RETURNS text[]
    LANGUAGE plpgsql
    AS $_$
DECLARE 
	group_list int[] := '{}';
	perm_list text[] := '{}';
	perms_sublist text [] := '{}';
	zz integer;
BEGIN
	group_list := find_groups($1);
	if group_list is null then
	   return perm_list;
	end if;

	RAISE NOTICE 'group: %', group_list;
	FOR zz IN 1 .. array_upper(group_list, 1) LOOP
	    perm_list := array_cat(perm_list, array(select permissions.mnemonic from role_assignments join role_permissions ON (role_assignments.role = role_permissions.role) join permissions on (role_permissions.mnemonic=permissions.mnemonic) WHERE role_assignments.sid=group_list[zz] group by permissions.mnemonic));
	    RAISE NOTICE 'group %   perm_list %', group_list[zz], perm_list;
	END LOOP;
	perms_sublist := array(select distinct perm_list[s.i]  from generate_series(array_lower(perm_list,1), array_upper(perm_list,1)) as s(i));
	return perms_sublist;
END;
$_$;


ALTER FUNCTION public.find_perms(integer) OWNER TO portaladmin;

--
-- Name: ga_fts_update(); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION ga_fts_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    begin
        new.fts_lexemes = to_tsvector(new.associated_gene_name) ||
            to_tsvector(new.associated_transcript_name) ||
	    to_tsvector(new.gene_id) ||
	    to_tsvector(new.transcript_id) ||
	    to_tsvector(new.description);
	    
        return new;
    end;
$$;


ALTER FUNCTION public.ga_fts_update() OWNER TO portaladmin;

--
-- Name: get_autocomplete_terms(text); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION get_autocomplete_terms(text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
   declare
      str text;
      tokens text[];
      i integer;
      j integer;
      t text;
      ret text;

   begin
      str := trim($1);
      ret := '';
      if length(str) > 4 then
         if strpos(str, '|') != 0 or strpos(str, ' ') != 0 then
            tokens := regexp_split_to_array(str, E'[\\|\\s]+');
         else
            tokens := array_append(tokens, str);
         end if;
         i := 0;
         for t in 1..array_length(tokens, 1) loop
            j := 4;
            while j < length(tokens[t]) loop
               ret := ret || ' ' || substr(tokens[t], 1, j);
               j := j + 1;
            end loop;
         end loop;

         return ret;
      end if;

      return '';
   end;
$_$;


ALTER FUNCTION public.get_autocomplete_terms(text) OWNER TO portaladmin;

--
-- Name: get_chip_type(text, text, text, text); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION get_chip_type(text, text, text, text) RETURNS integer
    LANGUAGE sql
    AS $_$ 
       select chip_type from assay_platforms 
       	      where species=$1 and manufacturer=$2 and platform=$3 and version=$4;       
$_$;


ALTER FUNCTION public.get_chip_type(text, text, text, text) OWNER TO portaladmin;

--
-- Name: get_database_id(text, text, text, text); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION get_database_id(text, text, text, text) RETURNS integer
    LANGUAGE sql
    AS $_$ 
       select an_database_id from annotation_databases 
       	      where genome_version=$1 and annotator=$2 and annotation_version=$3 and model_id=$4;       
$_$;


ALTER FUNCTION public.get_database_id(text, text, text, text) OWNER TO portaladmin;

--
-- Name: get_probe_annotation(integer, text); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION get_probe_annotation(ch_id integer, pr_id text) RETURNS record
    LANGUAGE plpgsql
    AS $$
    declare
        plat assay_platforms.manufacturer%type;
        tbl_name text;
        crsr refcursor;
        probe_annot record;
    begin
        -- Get the table name from the assay_platforms table.
        select manufacturer into plat from assay_platforms where chip_type=ch_id limit 1;
        tbl_name := plat_||'_probes';
        -- A bit of PG magic:  create a cursor so that we can decide which table to
        -- select from on-the-fly.
        open crsr for execute 'select * from '||tbl_name||' where probe_id='''||pr_id||'''';
        fetch crsr into probe_annot;
        close crsr;
        return probe_annot;
    end;
$$;


ALTER FUNCTION public.get_probe_annotation(ch_id integer, pr_id text) OWNER TO portaladmin;

--
-- Name: mul2(bigint, bigint); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION mul2(bigint, bigint) RETURNS bigint
    LANGUAGE plpgsql
    AS $_$
	DECLARE
			a ALIAS FOR $1;
			b ALIAS FOR $2;
		BEGIN
			return a * b;
		END;
$_$;


ALTER FUNCTION public.mul2(bigint, bigint) OWNER TO portaladmin;

--
-- Name: new_entrez_gene(); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION new_entrez_gene() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    declare
	--ensrec genome_annotations%rowtype;
        --syn text;
	pm_count int;
	eg_tsv tsvector;

    begin

        -- set the pubmed count in the NEW row.
        select count(*) from entrez_pubmed_map where ep_gene_id = new.eg_gene_id into pm_count;
	new.eg_pubmed_count = pm_count;
	
	-- update the fts columns of entrez_gene entry and corresponding genome_annotations entries.
        eg_tsv = to_tsvector(new.eg_description) ||
        -- OK: The Entrez numerical ID is included here, but you may not always want this.
            to_tsvector(new.eg_gene_id) ||
            to_tsvector(new.eg_symbol) || 
            to_tsvector(new.eg_full_name_from_nomenclature_authority) || 
            to_tsvector(new.eg_synonyms) ||
        -- OK: The following 3 lines add "auto-complete" terms for specified fields.
        -- Comment these out if you don't want or need these (don't forget to re-add the closing semicolon).
            to_tsvector(get_autocomplete_terms(new.eg_symbol)) ||
            to_tsvector(get_autocomplete_terms(new.eg_symbol_from_nomenclature_authority)) ||
            to_tsvector(get_autocomplete_terms(new.eg_synonyms));


	new.fts_lexemes = eg_tsv;

	update genome_annotations set fts_lexemes = fts_lexemes || eg_tsv
            where entrezgene_id = new.eg_gene_id;

	return new;

    end;

$$;


ALTER FUNCTION public.new_entrez_gene() OWNER TO portaladmin;

--
-- Name: new_genome_annotation(); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION new_genome_annotation() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    begin
        -- add rows to genome_mappings (gene) ...
        begin 
	    insert into genome_mappings values (new.db_id,new.gene_id,new.db_id,new.associated_gene_name);
        exception when unique_violation then
            -- do nothing
        end;

         begin 
	    if new.entrezgene_id != '' then
 	        insert into genome_mappings values (new.db_id,new.gene_id,new.db_id,new.entrezgene_id);
            end if;
         exception when unique_violation then
             -- do nothing
         end;

         begin 

	    if new.mgi_id != '' then
                insert into genome_mappings values (new.db_id,new.gene_id,new.db_id,new.mgi_id);
            end if;
         exception when unique_violation then
             -- do nothing
         end;

         begin 

	    insert into genome_mappings values (new.db_id,new.gene_id,new.db_id,new.gene_id);
        exception when unique_violation then
            -- do nothing
        end;

        begin 

            if new.refseq_dna_id != '' then
                insert into genome_mappings values (new.db_id,new.gene_id,new.db_id,new.refseq_dna_id);
            end if;
        exception when unique_violation then
            -- do nothing
        end;

        -- add rows to genome_mappings (transcript) ...
        begin 
            if new.associated_transcript_name != '' then
                insert into genome_mappings values (new.db_id,new.transcript_id,new.db_id,new.associated_transcript_name);
            end if;
        exception when unique_violation then
            -- do nothing
        end;

        begin 

	    insert into genome_mappings values (
	        new.db_id,new.transcript_id,new.db_id,new.transcript_id);
        exception when unique_violation then
            -- do nothing
        end;

        -- add rows to genome_locations (gene) ...
        begin
	    insert into genome_locations values (
	        new.db_id,new.gene_id,new.chromosome_name,new.strand,new.gene_start,new.gene_end,'gene');
        exception when unique_violation then
            -- do nothing
        end;

        -- add rows to genome_locations (transcript) ...
        begin
	    insert into genome_locations values (
	        new.db_id,new.transcript_id,new.chromosome_name,new.strand,
		    new.transcript_start,new.transcript_end,'transcript');
        exception when unique_violation then
            -- do nothing
        end;
	return NULL;

    end;

$$;


ALTER FUNCTION public.new_genome_annotation() OWNER TO portaladmin;

--
-- Name: new_probe_annotation(); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION new_probe_annotation() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    declare
	affy_seq affymetrix_probes_sequences%rowtype;

    begin
        begin
            if (tg_relname = 'illumina_probes') then
                insert into probe_annotations values (new.chip_type, new.probe_id, new.probe_sequence);
    
            elsif (tg_relname = 'affymetrix_probes') then
                select * from affymetrix_probes_sequences where affymetrix_probes_sequences.probe_id=new.probe_id into affy_seq limit 1;
                if (affy_seq is not null) then
                    insert into probe_annotations values (new.chip_type, 
                        new.probe_id||'.'||affy_seq.probe_interrogation_position, affy_seq.probe_sequence);
                else
                    insert into probe_annotations values (new.chip_type, new.probe_id, null);
                end if;

            else
                raise notice 'unknown probe manufacturer: %', tg_name;
        
            end if;
        
        exception when unique_violation then
            -- do nothing
        end;
    
        return null;

    end;

$$;


ALTER FUNCTION public.new_probe_annotation() OWNER TO portaladmin;

--
-- Name: new_probe_mapping(); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION new_probe_mapping() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

    declare
	affy_seq affymetrix_probes_sequences%rowtype;

    begin
        begin -- add mapping for probe <--> gene
            insert into probe_mappings values (new.chip_type, new.probe_id, new.db_id, new.gene_id, -1);
        exception when unique_violation then
            -- do nothing
        end;
    
        begin -- add mapping for probe <--> transcript
            insert into probe_mappings values (new.chip_type, new.probe_id, new.db_id, new.tran_id, -1);
        exception when unique_violation then
            -- do nothing
        end;
    
        return null;

    end;

$$;


ALTER FUNCTION public.new_probe_mapping() OWNER TO portaladmin;

--
-- Name: new_subject_trig(); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION new_subject_trig() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
        n_sid INTEGER;
	Vmaj INT := version_part(1);
	Vmin INT := version_part(2);
	tbl_name TEXT := CASE WHEN Vmaj = 7 OR Vmaj = 8 AND Vmin < 2 THEN TG_RELNAME ELSE TG_TABLE_NAME END;
	BEGIN
	    IF (tbl_name = 'subscribers') THEN -- 8.3 or later
		    INSERT INTO subjects VALUES (DEFAULT, 'INDIVIDUAL');
		    NEW.sid = lastval();
		    RETURN NEW;
            ELSEIF (tbl_name = 'groups') THEN -- 8.3 or later
            	    INSERT INTO subjects VALUES (DEFAULT, 'GROUP');
		    NEW.gid = lastval();
		    RETURN NEW;
	    ELSE
		    RAISE WARNING 'Trigger called on wrong table %', tbl_name;
		    RETURN NULL;
 	    END IF;
	END;
$$;


ALTER FUNCTION public.new_subject_trig() OWNER TO portaladmin;

--
-- Name: nfact(integer); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION nfact(integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
begin
	if ($1 <= 1) then
	   return 1;
	end if;

	return $1 * nfact($1 - 1);
end;
$_$;


ALTER FUNCTION public.nfact(integer) OWNER TO portaladmin;

--
-- Name: perm_code(integer); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION perm_code(subj integer) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
DECLARE 
	group_list int[] := '{}';
	perm_list int[] := '{}';
	perms_sublist int [] := '{}';
	perm_code bigint;
	zz integer;
BEGIN
	-- First, find all the groups of which this subject is a member.
	-- If there are no such groups, then return a permission code that
	-- is NEVER EVER USED.

	group_list := find_groups(subj);
	IF group_list IS NULL THEN
	   SELECT prime_factor FROM permissions where mnemonic='RESERVED1' into perm_code;
	   RETURN perm_code;
	END if;

	-- Subj is a member of at least one group.  Retrieve the set of permssions granted
	-- to this subj by virtue of membership in these groups.

	-- RAISE NOTICE 'group: %', group_list; -- DEBUG
	FOR zz IN array_lower(group_list,1) .. array_upper(group_list, 1) LOOP
	    perm_list := array_cat(perm_list, 
	    	  array(
	    	      SELECT permissions.prime_factor FROM role_assignments 
		      	     JOIN role_permissions ON (role_assignments.role = role_permissions.role) 
			     JOIN permissions ON (role_permissions.mnemonic=permissions.mnemonic) 
			     WHERE role_assignments.sid=group_list[zz] 
			     GROUP BY permissions.prime_factor
			 )
		);
	    -- RAISE NOTICE 'group %   perm_list %', group_list[zz], perm_list; -- DEBUG
	END LOOP;

	-- Reduce the list to a set of unique (i.e. non-duplicate) values, then
	-- compute their product.  
	-- (Yes, this looks UGLY, but I found it on-line and it works.)

	perms_sublist := array(
		      SELECT DISTINCT perm_list[s.i] AS pf  
		      	     FROM generate_series(array_lower(perm_list,1), array_upper(perm_list,1)) AS s(i)
			     );
	-- RAISE NOTICE 'perms_sublist %', perms_sublist; -- DEBUG
	perm_code = 1;
	FOR zz in array_lower(perms_sublist,1) .. array_upper(perms_sublist,1) LOOP
	    perm_code := perm_code * perms_sublist[zz];
	END LOOP;

	-- Return permission code result.

	RETURN perm_code;
	
END;
$$;


ALTER FUNCTION public.perm_code(subj integer) OWNER TO portaladmin;

--
-- Name: upsert_dsmeta(integer, text, text); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION upsert_dsmeta(dsid integer, key text, data text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    BEGIN
        -- first try to insert the row
        INSERT INTO dataset_metadata(ds_id, ds_name, ds_value) VALUES (dsid, key, data);
    EXCEPTION WHEN unique_violation THEN
        -- key already existed, try updating the row
        UPDATE dataset_metadata SET ds_value = data WHERE ds_name = key AND ds_id=dsid;
    END;
END;
$$;


ALTER FUNCTION public.upsert_dsmeta(dsid integer, key text, data text) OWNER TO portaladmin;

--
-- Name: vers_parts(); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION vers_parts() RETURNS integer[]
    LANGUAGE plpgsql
    AS $$
    DECLARE
	vers_str TEXT := split_part(version(), ' ', 2);
	vers_maj INT := split_part(vers_str, '.', 1)::INT;
	vers_min INT := split_part(vers_str, '.', 2)::INT;
	vers_minmin INT := split_part(vers_str, '.', 3)::INT;
	vp INT [] := '{}';
	BEGIN
	    RETURN array_append(array_append(array_append(vp, vers_maj), vers_min), vers_minmin); 
	END;
$$;


ALTER FUNCTION public.vers_parts() OWNER TO portaladmin;

--
-- Name: version_part(integer); Type: FUNCTION; Schema: public; Owner: portaladmin
--

CREATE FUNCTION version_part(integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
    DECLARE
        vp INT [] := vers_parts();
    BEGIN
	RETURN vp[$1]::text;
    END;
$_$;


ALTER FUNCTION public.version_part(integer) OWNER TO portaladmin;

--
-- Name: mul(bigint); Type: AGGREGATE; Schema: public; Owner: portaladmin
--

CREATE AGGREGATE mul(bigint) (
    SFUNC = mul2,
    STYPE = bigint,
    INITCOND = '1'
);


ALTER AGGREGATE public.mul(bigint) OWNER TO portaladmin;

--
-- Name: activity_serial; Type: SEQUENCE; Schema: public; Owner: portaladmin
--

CREATE SEQUENCE activity_serial
    START WITH 1000
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.activity_serial OWNER TO portaladmin;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: annotation_databases; Type: TABLE; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE TABLE annotation_databases (
    an_database_id integer NOT NULL,
    genome_version text NOT NULL,
    annotator text NOT NULL,
    annotation_version text NOT NULL,
    model_id text NOT NULL,
    external_link text,
    ucsc_reference text,
    external_link_previous_version text
);


ALTER TABLE public.annotation_databases OWNER TO portaladmin;

--
-- Name: assay_platforms; Type: TABLE; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE TABLE assay_platforms (
    chip_type integer NOT NULL,
    species text NOT NULL,
    manufacturer text,
    platform text,
    version text,
    platform_type text DEFAULT 'microarray'::text NOT NULL,
    min_y_axis real DEFAULT 0 NOT NULL,
    y_axis_label text DEFAULT 'Log2 Expression'::text NOT NULL,
    y_axis_label_description text DEFAULT ''::text NOT NULL,
    mapping_id integer DEFAULT 0 NOT NULL,
    log_2 boolean DEFAULT true,
    probe_name text DEFAULT 'Probe'::text NOT NULL,
    default_graph_title text DEFAULT 'Gene Expression Graph'::text NOT NULL
);


ALTER TABLE public.assay_platforms OWNER TO portaladmin;

--
-- Name: biosamples; Type: TABLE; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE TABLE biosamples (
    chip_id text NOT NULL,
    chip_type integer NOT NULL,
    ds_id integer NOT NULL,
    alias text NOT NULL,
    archive_accession_id text NOT NULL,
    sample_id text NOT NULL
);


ALTER TABLE public.biosamples OWNER TO portaladmin;

--
-- Name: biosamples_metadata; Type: TABLE; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE TABLE biosamples_metadata (
    chip_type integer NOT NULL,
    chip_id text NOT NULL,
    md_name text NOT NULL,
    md_value text NOT NULL,
    ds_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.biosamples_metadata OWNER TO portaladmin;

--
-- Name: biosamples_metadata_backup; Type: TABLE; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE TABLE biosamples_metadata_backup (
    chip_type integer,
    chip_id text,
    md_name text,
    md_value text,
    ds_id integer
);


ALTER TABLE public.biosamples_metadata_backup OWNER TO portaladmin;

--
-- Name: bm_bk; Type: TABLE; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE TABLE bm_bk (
    chip_type integer,
    chip_id text,
    md_name text,
    md_value text,
    ds_id integer
);


ALTER TABLE public.bm_bk OWNER TO portaladmin;

--
-- Name: dataset_metadata; Type: TABLE; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE TABLE dataset_metadata (
    ds_id integer NOT NULL,
    ds_name text NOT NULL,
    ds_value text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.dataset_metadata OWNER TO portaladmin;

--
-- Name: dataset_metadata_backup; Type: TABLE; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE TABLE dataset_metadata_backup (
    ds_id integer,
    ds_name text,
    ds_value text
);


ALTER TABLE public.dataset_metadata_backup OWNER TO portaladmin;

--
-- Name: datasets; Type: TABLE; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE TABLE datasets (
    id integer NOT NULL,
    lab text DEFAULT 'UNKNOWN'::text NOT NULL,
    dtg timestamp without time zone DEFAULT now() NOT NULL,
    handle text NOT NULL,
    published boolean DEFAULT true NOT NULL,
    private boolean DEFAULT true NOT NULL,
    chip_type integer DEFAULT 0 NOT NULL,
    min_y_axis text DEFAULT ''::text NOT NULL,
    show_yugene boolean DEFAULT true NOT NULL,
    show_limited boolean DEFAULT false NOT NULL,
    db_id integer DEFAULT 0,
    number_of_samples integer,
    data_type_id integer DEFAULT 0 NOT NULL,
    mapping_id integer DEFAULT 0 NOT NULL,
    log_2 boolean DEFAULT false NOT NULL
);


ALTER TABLE public.datasets OWNER TO portaladmin;

--
-- Name: datasets_bk; Type: TABLE; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE TABLE datasets_bk (
    id integer NOT NULL,
    lab text DEFAULT 'UNKNOWN'::text NOT NULL,
    dtg timestamp without time zone DEFAULT now() NOT NULL,
    handle text NOT NULL,
    published boolean DEFAULT true NOT NULL,
    private boolean DEFAULT true NOT NULL,
    chip_type integer DEFAULT 0 NOT NULL,
    min_y_axis text DEFAULT ''::text NOT NULL,
    show_yugene boolean DEFAULT true NOT NULL,
    show_limited boolean DEFAULT false NOT NULL,
    db_id integer DEFAULT 0,
    number_of_samples integer,
    data_type_id integer DEFAULT 0 NOT NULL,
    mapping_id integer DEFAULT 0 NOT NULL,
    log_2 boolean DEFAULT false NOT NULL
);


ALTER TABLE public.datasets_bk OWNER TO portaladmin;

--
-- Name: genome_annotations; Type: TABLE; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE TABLE genome_annotations (
    db_id integer NOT NULL,
    gene_id text NOT NULL,
    description text,
    chromosome_name text,
    gene_start integer DEFAULT 0,
    gene_end integer DEFAULT 0,
    strand integer DEFAULT 0,
    band text,
    associated_gene_name text,
    associated_gene_synonym text,
    associated_gene_db text,
    gene_biotype text,
    source text,
    gene_status text,
    entrezgene_id text,
    mgi_id text,
    refseq_dna_id text,
    fts_lexemes tsvector
);


ALTER TABLE public.genome_annotations OWNER TO portaladmin;

--
-- Name: gid_seq; Type: SEQUENCE; Schema: public; Owner: portaladmin
--

CREATE SEQUENCE gid_seq
    START WITH 3
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.gid_seq OWNER TO portaladmin;

--
-- Name: id_auto_increment; Type: SEQUENCE; Schema: public; Owner: portaladmin
--

CREATE SEQUENCE id_auto_increment
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.id_auto_increment OWNER TO portaladmin;

--
-- Name: subject_serial; Type: SEQUENCE; Schema: public; Owner: portaladmin
--

CREATE SEQUENCE subject_serial
    START WITH 3000
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.subject_serial OWNER TO portaladmin;

--
-- Name: subject_type_serial; Type: SEQUENCE; Schema: public; Owner: portaladmin
--

CREATE SEQUENCE subject_type_serial
    START WITH 1000
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.subject_type_serial OWNER TO portaladmin;

SET search_path = stemformatics, pg_catalog;

--
-- Name: annotation_filters; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE annotation_filters (
    id integer NOT NULL,
    uid integer NOT NULL,
    name text NOT NULL,
    json_filter text NOT NULL
);


ALTER TABLE stemformatics.annotation_filters OWNER TO portaladmin;

--
-- Name: annotation_filters_id_seq; Type: SEQUENCE; Schema: stemformatics; Owner: portaladmin
--

CREATE SEQUENCE annotation_filters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE stemformatics.annotation_filters_id_seq OWNER TO portaladmin;

--
-- Name: annotation_filters_id_seq; Type: SEQUENCE OWNED BY; Schema: stemformatics; Owner: portaladmin
--

ALTER SEQUENCE annotation_filters_id_seq OWNED BY annotation_filters.id;


--
-- Name: audit_log; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE audit_log (
    id integer,
    ref_type text,
    ref_id text,
    uid integer,
    date_created timestamp without time zone DEFAULT now(),
    controller text,
    action text,
    ip_address text,
    extra_ref_id text,
    extra_ref_type text
);


ALTER TABLE stemformatics.audit_log OWNER TO portaladmin;

--
-- Name: audit_log_id_seq; Type: SEQUENCE; Schema: stemformatics; Owner: portaladmin
--

CREATE SEQUENCE audit_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE stemformatics.audit_log_id_seq OWNER TO portaladmin;

--
-- Name: audit_log_id_seq; Type: SEQUENCE OWNED BY; Schema: stemformatics; Owner: portaladmin
--

ALTER SEQUENCE audit_log_id_seq OWNED BY audit_log.id;


--
-- Name: configs; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE configs (
    id integer,
    ref_type text,
    ref_id text
);


ALTER TABLE stemformatics.configs OWNER TO portaladmin;

--
-- Name: config_id_seq; Type: SEQUENCE; Schema: stemformatics; Owner: portaladmin
--

CREATE SEQUENCE config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE stemformatics.config_id_seq OWNER TO portaladmin;

--
-- Name: config_id_seq; Type: SEQUENCE OWNED BY; Schema: stemformatics; Owner: portaladmin
--

ALTER SEQUENCE config_id_seq OWNED BY configs.id;


--
-- Name: dataset_download_audits; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE dataset_download_audits (
    uid integer,
    ds_id integer,
    download_type text,
    ip_address text,
    date_created timestamp without time zone DEFAULT now(),
    permission_used text
);


ALTER TABLE stemformatics.dataset_download_audits OWNER TO portaladmin;

--
-- Name: export_key; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE export_key (
    uid integer,
    key text,
    date_created timestamp without time zone DEFAULT now()
);


ALTER TABLE stemformatics.export_key OWNER TO portaladmin;

--
-- Name: feature_mappings; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE feature_mappings (
    db_id integer NOT NULL,
    mapping_id integer NOT NULL,
    from_type text NOT NULL,
    from_id text NOT NULL,
    to_type text NOT NULL,
    to_id text NOT NULL
);


ALTER TABLE stemformatics.feature_mappings OWNER TO portaladmin;

--
-- Name: gene_set_items; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE gene_set_items (
    id bigint NOT NULL,
    gene_set_id bigint NOT NULL,
    gene_id text NOT NULL
);


ALTER TABLE stemformatics.gene_set_items OWNER TO portaladmin;

--
-- Name: gene_set_items_id_seq; Type: SEQUENCE; Schema: stemformatics; Owner: portaladmin
--

CREATE SEQUENCE gene_set_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE stemformatics.gene_set_items_id_seq OWNER TO portaladmin;

--
-- Name: gene_set_items_id_seq; Type: SEQUENCE OWNED BY; Schema: stemformatics; Owner: portaladmin
--

ALTER SEQUENCE gene_set_items_id_seq OWNED BY gene_set_items.id;


--
-- Name: gene_sets; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE gene_sets (
    id bigint NOT NULL,
    gene_set_name text NOT NULL,
    db_id integer NOT NULL,
    description text,
    uid integer NOT NULL,
    gene_set_type text DEFAULT ''::text NOT NULL,
    needs_attention boolean DEFAULT false NOT NULL
);


ALTER TABLE stemformatics.gene_sets OWNER TO portaladmin;

--
-- Name: gene_sets_id_seq; Type: SEQUENCE; Schema: stemformatics; Owner: portaladmin
--

CREATE SEQUENCE gene_sets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE stemformatics.gene_sets_id_seq OWNER TO portaladmin;

--
-- Name: gene_sets_id_seq; Type: SEQUENCE OWNED BY; Schema: stemformatics; Owner: portaladmin
--

ALTER SEQUENCE gene_sets_id_seq OWNED BY gene_sets.id;


--
-- Name: group_configs; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE group_configs (
    gid integer NOT NULL,
    config_type text NOT NULL,
    config_name text NOT NULL,
    config_value text NOT NULL,
    db_id integer
);


ALTER TABLE stemformatics.group_configs OWNER TO portaladmin;

--
-- Name: group_users; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE group_users (
    gid integer NOT NULL,
    uid integer NOT NULL,
    role text NOT NULL
);


ALTER TABLE stemformatics.group_users OWNER TO portaladmin;

--
-- Name: groups; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE groups (
    gid integer DEFAULT nextval('public.gid_seq'::regclass) NOT NULL,
    group_name text NOT NULL
);


ALTER TABLE stemformatics.groups OWNER TO portaladmin;

--
-- Name: groups_gid_seq; Type: SEQUENCE; Schema: stemformatics; Owner: portaladmin
--

CREATE SEQUENCE groups_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE stemformatics.groups_gid_seq OWNER TO portaladmin;

--
-- Name: groups_gid_seq; Type: SEQUENCE OWNED BY; Schema: stemformatics; Owner: portaladmin
--

ALTER SEQUENCE groups_gid_seq OWNED BY groups.gid;


--
-- Name: jobs; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE jobs (
    job_id integer NOT NULL,
    analysis integer NOT NULL,
    status integer NOT NULL,
    dataset_id integer NOT NULL,
    gene_set_id integer NOT NULL,
    uid integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    use_cls boolean,
    use_gct boolean,
    reference_type text,
    reference_id text,
    gene text,
    probe text,
    comparison_type text,
    finished timestamp without time zone,
    remove_chip_ids text,
    options text
);


ALTER TABLE stemformatics.jobs OWNER TO portaladmin;

--
-- Name: jobs_job_id_seq; Type: SEQUENCE; Schema: stemformatics; Owner: portaladmin
--

CREATE SEQUENCE jobs_job_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE stemformatics.jobs_job_id_seq OWNER TO portaladmin;

--
-- Name: jobs_job_id_seq; Type: SEQUENCE OWNED BY; Schema: stemformatics; Owner: portaladmin
--

ALTER SEQUENCE jobs_job_id_seq OWNED BY jobs.job_id;


--
-- Name: notifications; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE notifications (
    id integer NOT NULL,
    notify_type text NOT NULL,
    uid text NOT NULL,
    subject text NOT NULL,
    body text NOT NULL,
    unread boolean DEFAULT true,
    creation_date timestamp without time zone
);


ALTER TABLE stemformatics.notifications OWNER TO portaladmin;

--
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: stemformatics; Owner: portaladmin
--

CREATE SEQUENCE notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE stemformatics.notifications_id_seq OWNER TO portaladmin;

--
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: stemformatics; Owner: portaladmin
--

ALTER SEQUENCE notifications_id_seq OWNED BY notifications.id;


--
-- Name: override_private_datasets; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE override_private_datasets (
    ds_id integer NOT NULL,
    uid integer DEFAULT 0 NOT NULL,
    role text DEFAULT 'view'::text NOT NULL,
    object_type text DEFAULT ''::text NOT NULL,
    object_id text DEFAULT ''::text NOT NULL
);


ALTER TABLE stemformatics.override_private_datasets OWNER TO portaladmin;

--
-- Name: private_gene_sets_update_archive; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE private_gene_sets_update_archive (
    id integer DEFAULT nextval('public.id_auto_increment'::regclass) NOT NULL,
    gene_set_id integer NOT NULL,
    gene_set_name text NOT NULL,
    user_id integer NOT NULL,
    user_name text NOT NULL,
    species_db_id integer NOT NULL,
    old_ens_id text NOT NULL,
    new_ens_id text DEFAULT ''::text,
    ens_id_status public.ens_id_status,
    old_ens_version integer NOT NULL,
    new_ens_version integer NOT NULL,
    old_ogs text NOT NULL,
    new_ogs text,
    old_entrez_id integer,
    new_entrez_id integer,
    s4m_update_version double precision NOT NULL
);


ALTER TABLE stemformatics.private_gene_sets_update_archive OWNER TO portaladmin;

--
-- Name: shared_resources; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE shared_resources (
    share_type text NOT NULL,
    share_id integer NOT NULL,
    from_uid integer NOT NULL,
    to_uid integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    expiry_date timestamp without time zone
);


ALTER TABLE stemformatics.shared_resources OWNER TO portaladmin;

--
-- Name: stats_datasetsample_count; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE stats_datasetsample_count (
    ds_id integer NOT NULL,
    chip_type integer NOT NULL,
    samples integer NOT NULL
);


ALTER TABLE stemformatics.stats_datasetsample_count OWNER TO portaladmin;

--
-- Name: stats_datasetsummary; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE stats_datasetsummary (
    ds_id integer NOT NULL,
    md_name text NOT NULL,
    md_value text NOT NULL,
    count integer NOT NULL
);


ALTER TABLE stemformatics.stats_datasetsummary OWNER TO portaladmin;

--
-- Name: stats_detectedprobes_count; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE stats_detectedprobes_count (
    ds_id integer NOT NULL,
    chip_type integer NOT NULL,
    probe_id text NOT NULL,
    detected integer DEFAULT 0 NOT NULL
);


ALTER TABLE stemformatics.stats_detectedprobes_count OWNER TO portaladmin;

--
-- Name: stats_genedetected; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE stats_genedetected (
    database_id integer NOT NULL,
    identifier text NOT NULL,
    detected_samples integer NOT NULL,
    samples integer NOT NULL
);


ALTER TABLE stemformatics.stats_genedetected OWNER TO portaladmin;

--
-- Name: transcript_annotations; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE transcript_annotations (
    db_id integer NOT NULL,
    transcript_id text NOT NULL,
    gene_id text NOT NULL,
    transcript_name text NOT NULL,
    protein_length integer DEFAULT 0 NOT NULL,
    signal_peptide boolean DEFAULT false,
    tm_domain boolean DEFAULT false,
    targeted_mirna boolean DEFAULT false
);


ALTER TABLE stemformatics.transcript_annotations OWNER TO portaladmin;

--
-- Name: users; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE users (
    uid integer NOT NULL,
    username text NOT NULL,
    password text,
    organisation text,
    full_name text,
    status integer DEFAULT 0,
    created timestamp without time zone DEFAULT now(),
    confirm_code text,
    password_expiry timestamp without time zone,
    send_email_marketing boolean DEFAULT false NOT NULL,
    send_email_job_notifications boolean DEFAULT false NOT NULL,
    is_admin boolean DEFAULT false NOT NULL,
    role text DEFAULT 'normal'::text NOT NULL,
    base_export_key text
);


ALTER TABLE stemformatics.users OWNER TO portaladmin;

--
-- Name: users_metadata; Type: TABLE; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE TABLE users_metadata (
    uid integer NOT NULL,
    md_name text NOT NULL,
    md_value text NOT NULL
);


ALTER TABLE stemformatics.users_metadata OWNER TO portaladmin;

--
-- Name: users_uid_seq; Type: SEQUENCE; Schema: stemformatics; Owner: portaladmin
--

CREATE SEQUENCE users_uid_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE stemformatics.users_uid_seq OWNER TO portaladmin;

--
-- Name: users_uid_seq; Type: SEQUENCE OWNED BY; Schema: stemformatics; Owner: portaladmin
--

ALTER SEQUENCE users_uid_seq OWNED BY users.uid;


--
-- Name: id; Type: DEFAULT; Schema: stemformatics; Owner: portaladmin
--

ALTER TABLE ONLY annotation_filters ALTER COLUMN id SET DEFAULT nextval('annotation_filters_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: stemformatics; Owner: portaladmin
--

ALTER TABLE ONLY audit_log ALTER COLUMN id SET DEFAULT nextval('audit_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: stemformatics; Owner: portaladmin
--

ALTER TABLE ONLY configs ALTER COLUMN id SET DEFAULT nextval('config_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: stemformatics; Owner: portaladmin
--

ALTER TABLE ONLY gene_set_items ALTER COLUMN id SET DEFAULT nextval('gene_set_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: stemformatics; Owner: portaladmin
--

ALTER TABLE ONLY gene_sets ALTER COLUMN id SET DEFAULT nextval('gene_sets_id_seq'::regclass);


--
-- Name: job_id; Type: DEFAULT; Schema: stemformatics; Owner: portaladmin
--

ALTER TABLE ONLY jobs ALTER COLUMN job_id SET DEFAULT nextval('jobs_job_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: stemformatics; Owner: portaladmin
--

ALTER TABLE ONLY notifications ALTER COLUMN id SET DEFAULT nextval('notifications_id_seq'::regclass);


--
-- Name: uid; Type: DEFAULT; Schema: stemformatics; Owner: portaladmin
--

ALTER TABLE ONLY users ALTER COLUMN uid SET DEFAULT nextval('users_uid_seq'::regclass);


SET search_path = public, pg_catalog;

--
-- Name: annotation_databases_pkey; Type: CONSTRAINT; Schema: public; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY annotation_databases
    ADD CONSTRAINT annotation_databases_pkey PRIMARY KEY (an_database_id);


--
-- Name: assay_platforms_pkey; Type: CONSTRAINT; Schema: public; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY assay_platforms
    ADD CONSTRAINT assay_platforms_pkey PRIMARY KEY (chip_type);


--
-- Name: biosamples_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY biosamples_metadata
    ADD CONSTRAINT biosamples_metadata_pkey PRIMARY KEY (chip_type, chip_id, md_name, ds_id);


--
-- Name: biosamples_pkey; Type: CONSTRAINT; Schema: public; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY biosamples
    ADD CONSTRAINT biosamples_pkey PRIMARY KEY (chip_id, ds_id);


--
-- Name: dataset_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY dataset_metadata
    ADD CONSTRAINT dataset_metadata_pkey PRIMARY KEY (ds_id, ds_name, ds_value);


--
-- Name: datasets_handle_key; Type: CONSTRAINT; Schema: public; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY datasets_bk
    ADD CONSTRAINT datasets_handle_key UNIQUE (handle);


--
-- Name: datasets_pkey; Type: CONSTRAINT; Schema: public; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY datasets_bk
    ADD CONSTRAINT datasets_pkey PRIMARY KEY (id);


--
-- Name: genome_annotations_new_pkey; Type: CONSTRAINT; Schema: public; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY genome_annotations
    ADD CONSTRAINT genome_annotations_new_pkey PRIMARY KEY (db_id, gene_id);


--
-- Name: id2; Type: CONSTRAINT; Schema: public; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY datasets
    ADD CONSTRAINT id2 PRIMARY KEY (id);


SET search_path = stemformatics, pg_catalog;

--
-- Name: annotation_filters_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY annotation_filters
    ADD CONSTRAINT annotation_filters_pkey PRIMARY KEY (id);


--
-- Name: feature_mappings_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY feature_mappings
    ADD CONSTRAINT feature_mappings_pkey PRIMARY KEY (db_id, mapping_id, from_type, from_id, to_type, to_id);


--
-- Name: group_configs_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY group_configs
    ADD CONSTRAINT group_configs_pkey PRIMARY KEY (gid, config_type, config_name);


--
-- Name: group_users_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY group_users
    ADD CONSTRAINT group_users_pkey PRIMARY KEY (gid, uid);


--
-- Name: groups_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (gid);


--
-- Name: job_id; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY jobs
    ADD CONSTRAINT job_id PRIMARY KEY (job_id);


--
-- Name: notifications_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: override_private_datasets_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY override_private_datasets
    ADD CONSTRAINT override_private_datasets_pkey PRIMARY KEY (ds_id, object_type, object_id);


--
-- Name: pk_gene_set_items; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY gene_set_items
    ADD CONSTRAINT pk_gene_set_items PRIMARY KEY (id);


--
-- Name: pk_gene_sets; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY gene_sets
    ADD CONSTRAINT pk_gene_sets PRIMARY KEY (id);


--
-- Name: private_gene_sets_update_archive_gene_set_id_key; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY private_gene_sets_update_archive
    ADD CONSTRAINT private_gene_sets_update_archive_gene_set_id_key UNIQUE (gene_set_id, user_id, old_ens_id, s4m_update_version);


--
-- Name: private_gene_sets_update_archive_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY private_gene_sets_update_archive
    ADD CONSTRAINT private_gene_sets_update_archive_pkey PRIMARY KEY (id);


--
-- Name: shared_resources_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY shared_resources
    ADD CONSTRAINT shared_resources_pkey PRIMARY KEY (share_type, share_id, from_uid, to_uid);


--
-- Name: stats_datasetsummary_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY stats_datasetsummary
    ADD CONSTRAINT stats_datasetsummary_pkey PRIMARY KEY (ds_id, md_name, md_value);


--
-- Name: stats_detectedprobes_count_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY stats_detectedprobes_count
    ADD CONSTRAINT stats_detectedprobes_count_pkey PRIMARY KEY (ds_id, chip_type, probe_id);


--
-- Name: stats_genedetected_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY stats_genedetected
    ADD CONSTRAINT stats_genedetected_pkey PRIMARY KEY (database_id, identifier);


--
-- Name: stats_samplesprobes_count_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY stats_datasetsample_count
    ADD CONSTRAINT stats_samplesprobes_count_pkey PRIMARY KEY (ds_id, chip_type);


--
-- Name: transcript_annotations_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY transcript_annotations
    ADD CONSTRAINT transcript_annotations_pkey PRIMARY KEY (db_id, transcript_id);


--
-- Name: unique_gene_items; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY gene_set_items
    ADD CONSTRAINT unique_gene_items UNIQUE (gene_set_id, gene_id);


--
-- Name: unique_gene_sets; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY gene_sets
    ADD CONSTRAINT unique_gene_sets UNIQUE (gene_set_name, uid, db_id);


--
-- Name: username; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT username PRIMARY KEY (username);


--
-- Name: users_metadata_pkey; Type: CONSTRAINT; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

ALTER TABLE ONLY users_metadata
    ADD CONSTRAINT users_metadata_pkey PRIMARY KEY (uid, md_name);


SET search_path = public, pg_catalog;

--
-- Name: biosamples_arcive_acc_id; Type: INDEX; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE INDEX biosamples_arcive_acc_id ON biosamples USING btree (archive_accession_id);


--
-- Name: biosamples_chip_id; Type: INDEX; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE INDEX biosamples_chip_id ON biosamples USING btree (chip_id);


--
-- Name: biosamples_chip_type; Type: INDEX; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE INDEX biosamples_chip_type ON biosamples USING btree (chip_type);


--
-- Name: biosamples_ds_id; Type: INDEX; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE INDEX biosamples_ds_id ON biosamples USING btree (ds_id);


--
-- Name: chip_idx; Type: INDEX; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE INDEX chip_idx ON biosamples_metadata USING btree (chip_id);


--
-- Name: chip_type_idx; Type: INDEX; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE INDEX chip_type_idx ON datasets USING btree (chip_type);


--
-- Name: db_id_idx; Type: INDEX; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE INDEX db_id_idx ON datasets USING btree (db_id);


--
-- Name: ds_id_chip_id; Type: INDEX; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE INDEX ds_id_chip_id ON biosamples_metadata USING btree (ds_id, chip_id);


--
-- Name: private_idx; Type: INDEX; Schema: public; Owner: portaladmin; Tablespace: 
--

CREATE INDEX private_idx ON datasets USING btree (private);


SET search_path = stemformatics, pg_catalog;

--
-- Name: annotation_filters_uid; Type: INDEX; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE INDEX annotation_filters_uid ON annotation_filters USING btree (uid);


--
-- Name: date_created_export_key; Type: INDEX; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE INDEX date_created_export_key ON export_key USING btree (date_created);


--
-- Name: to_idx; Type: INDEX; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE INDEX to_idx ON feature_mappings USING btree (to_type, to_id, from_type);


--
-- Name: uid_export_key; Type: INDEX; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE INDEX uid_export_key ON export_key USING btree (uid);


--
-- Name: unique_key; Type: INDEX; Schema: stemformatics; Owner: portaladmin; Tablespace: 
--

CREATE UNIQUE INDEX unique_key ON export_key USING btree (key);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

